import os
import json
from flask import Flask, request, Response
ASSETS_DIR = os.path.dirname(os.path.abspath(__file__))
app = Flask(__name__)


@app.route('/api/v1/events/ingest', methods=['POST'], defaults={"agentId": "127.0.0.1"})
@app.route('/api/v1/events/ingest/<agentId>', methods=['POST'])
def ingest_events(agentId):
    count = 0
    req = request.get_json()
    if req.get('events') is list() and req.get('events')[0] is dict():
        if req.get('text') is str():
            if not req.get('timestamp'):
                events = req.get('events')
                for event in events:
                    if event.get('fields') is list() and event.get('fields')[0] is dict():
                        count += 1
                    else:
                        continue
                return Response(json.dumps({"status": "failed"}), mimetype="application/json", status=400)
    data = {
        "ingested": count,
        "message": "events ingested",
        "status": "ok",
        "agent": agentId
    }
    return Response(json.dumps(data), mimetype="application/json", status=200)


if __name__ == "__main__":
    app.run(debug=False, ssl_context=('server.crt', 'server.key'), port=5001)
