import unittest
import requests


class IngestTest(unittest.TestCase):
    def test_ingestion_mock_post(self):
        data = {
            "events": [{
                "fields": [
                    {"name": "test", "content": "something"},
                    {"name": "else", "content": "elselse"}
                ],
                "message": "test this message"
            }]
        }
        r = requests.post("https://localhost:5001/api/v1/events/ingest", json=data, verify=False)
        self.assertEqual(None, r.raise_for_status())

    def test_ingest_post(self):
        data = {
            "message": "test message on thing",
            "new": "valueshere",
            "test": "keydata"
        }
        r = requests.post("http://localhost:5000/vrli", json=data)
        self.assertEqual(None, r.raise_for_status())


unittest.main()
