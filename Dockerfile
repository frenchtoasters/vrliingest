FROM python:3.8-slim

WORKDIR /opt/vrli
COPY requirements.txt /opt/vrli/
RUN python -m pip install -r requirements.txt

COPY . /opt/vrli
EXPOSE 5000

ENTRYPOINT ["gunicorn", "-c", "./gunicorn_config.py"]