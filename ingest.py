import requests
import os
import json
from flask import Flask, request, Response
app = Flask(__name__)
vrli_address = os.environ['VRLI_ADDRESS']


@app.route('/vrli', methods=['POST'], defaults={"message_field": "message", "agent_id": "127.0.0.1"})
@app.route('/vrli/<message_field>', methods=['POST'], defaults={"agent_id": "127.0.0.1"})
@app.route('/vrli/<message_field>/<agent_id>', methods=['POST'])
def ingest(message_field, agent_id):
    """
    Processes a single event a time
    Payload accepted: {"key": "value", "key2":"value2"}
    :return:
    """
    payload = request.get_json()
    data = {
        "events": [{
            "fields": [],
            "text": payload[message_field]
        }]
    }
    for key, value in payload.items():
        if key is not message_field:
            data['events'][0]['fields'].append({"name": key, "content": value})

    r = requests.post(url=f"https://{vrli_address}/api/v1/events/ingest/{agent_id}", json=data, verify=False)
    if r.raise_for_status():
        return Response(json.dumps({"status": "Failed"}), status=r.status_code, mimetype="application/json")
    return Response(json.dumps({"status": "Success"}), status=r.status_code, mimetype="application/json")


if __name__ == "__main__":
    app.run()
